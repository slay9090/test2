import { sessionToken } from './authentication/sessionToken'
import { refreshToken } from './authentication/refreshToken'
import { authUtils } from './authentication/utils'

export function authenticationTool({
                                    apiGetAnonToken,
                                    apiRefreshAnonToken,
                                    apiGetAuthToken,
                                    apiRefreshAuthToken,
                                    refreshTokenKey,
                                    sessionTokenKey,
                                  }) {
  const { getRefreshToken, removeRefreshToken, setRefreshToken }
    = refreshToken(refreshTokenKey)
  const { setSessionToken, getSessionToken } = sessionToken(sessionTokenKey)
  const { hasAuthorizedToken } = authUtils()
  const isAuth = useState('isAuth')

  const updateTokens = async (credentials?) => {
    const setFailure = () => {
      setSessionToken(null)
      removeRefreshToken()
      isAuth.value = false
    }
    /** если авторизация записываем токены, выходим **/
    if (credentials) {
      try {
        const { access_token, refresh_token } = await apiGetAuthToken(credentials)
        setSessionToken(access_token)
        setRefreshToken(refresh_token)
        isAuth.value = true
      }
      catch (e) {
        setFailure()
      }
      return { access_token: getSessionToken() }
    }
    /** если нет рефреш токена, получаем анонимные, выходим **/
    if (!getRefreshToken()) {
      try {
        const { access_token, refresh_token } = await apiGetAnonToken()
        setSessionToken(access_token)
        setRefreshToken(refresh_token)
        isAuth.value = false
      }
      catch (e) {
        setFailure()
      }
      return { access_token: getSessionToken() }
    }
    /** чекаем какой рефреш токен анон или авториз, перезаписываем токен сессии **/
    const isAuthRefreshToken = hasAuthorizedToken(getRefreshToken())

    if (isAuthRefreshToken) {
      try {
        const { access_token, refresh_token } = await apiRefreshAuthToken(getRefreshToken())
        setSessionToken(access_token)
        setRefreshToken(refresh_token)
        isAuth.value = true
      }
      catch (e) {
        setFailure()
      }
    }
    else {
      try {
        const { access_token, refresh_token } = await apiRefreshAnonToken(getRefreshToken())
        setSessionToken(access_token)
        setRefreshToken(refresh_token)
        isAuth.value = false
      }
      catch (e) {
        setFailure()
      }
    }
    return { access_token: getSessionToken() }
  }

  const removeTokens = () => {
    setSessionToken(null)
    removeRefreshToken()
    isAuth.value = false
  }

  return {
    updateTokens,
    getSessionToken,
    getRefreshToken,
    isAuth,
    removeTokens,
    setSessionToken,
    setRefreshToken,
  }
}
