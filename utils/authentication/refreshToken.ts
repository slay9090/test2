export function refreshToken(refreshTokenKey: string) {
  const refreshToken = useCookie(refreshTokenKey)

  const setRefreshToken = (token: string) => {
    refreshToken.value = token
  }
  const getRefreshToken = () => {
    return refreshToken.value
  }
  const removeRefreshToken = () => {
    refreshToken.value = null
  }

  return {
    setRefreshToken,
    getRefreshToken,
    removeRefreshToken,
  }
}