export function sessionToken(sessionTokenKey: string) {
  // const sessionToken = useCookie(sessionTokenKey)

  const useSessionToken = useState('sessionToken')

  const setSessionToken = (token) => {
    useSessionToken.value = token
  }
  const getSessionToken = () => {
    return useSessionToken.value
  }

  return {
    setSessionToken,
    getSessionToken,
  }
}